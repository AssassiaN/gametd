﻿using gameTD.entity;
using gameTD.render;
using gameTD.td;
using SharpGL.SceneGraph;
using SharpGL.WPF;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Windows.Input;

namespace gameTD.screen
{
    abstract class BaseScreen : Drawer
    {
        protected OpenGLControl mainCanva;
        protected RenderHelper gameRH;
        protected GameTD gameTD;
        //список нажатых обьектов
        protected ConcurrentQueue<string> objClick;
        //флвг загрузки обьектов в списки
        protected bool loadObj;

        public BaseScreen(OpenGLControl mainCanva, RenderHelper gameRenderHelper, GameTD gameTD)
        {
            this.mainCanva = mainCanva;
            this.gameRH = gameRenderHelper;
            this.gameTD = gameTD;
            objClick = new ConcurrentQueue<string>();

            loadObj = true;
        }

        public abstract void OpenGLDraw(object sender, OpenGLEventArgs e);

        public abstract void onMouseDown(object sender, MouseButtonEventArgs e);

        protected abstract void loadObjects();

        protected abstract void gameProcessor();
    }
}
