﻿using gameTD.entity;
using gameTD.render;
using gameTD.td;
using SharpGL;
using SharpGL.SceneGraph;
using SharpGL.WPF;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace gameTD.screen
{
    class MainMenuScreen : BaseScreen
    {
        //списки всех обьектов на экране
        protected List<MenuObject> objMainMenu;

        public MainMenuScreen(OpenGLControl mainCanva, RenderHelper gameRenderHelper, GameTD gameTD) : base(mainCanva, gameRenderHelper, gameTD)
        {
            objMainMenu = new List<MenuObject>();
        }

        public override void OpenGLDraw(object sender, OpenGLEventArgs e)
        {
            loadObjects();
            gameProcessor();

            OpenGL gl = e.OpenGL;

            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);

            gl.MatrixMode(OpenGL.GL_MODELVIEW);
            gl.LoadIdentity();

            foreach (MenuObject obj in objMainMenu)
            {
                obj.draw(gl, gameRH);
            }

            gl.Flush();
        }

        protected override void  gameProcessor()
        {
            if (objClick.Count > 0)
            {
                string objEvent = "";
                if (objClick.TryDequeue(out objEvent))
                {
                    if (objEvent != "")
                    {
                        if (objEvent == "StartBtn")
                            gameTD.changeScreen(gameTD.getGameScreen());
                        if (objEvent == "HelpBtn")
                            MessageBox.Show("HelpBtn");
                        if (objEvent == "ExitBtn")
                            gameTD.exitTD();
                    }
                }
            }
        }

        protected override void loadObjects()
        {
            if (loadObj)
            {
                float margineBtn = 0.1f;
                float widthBtn = 0.25f;
                float heightBtn = widthBtn / 2;
                float multBtn = heightBtn / 2 + margineBtn;

                objMainMenu.Add(new MenuObject(gameRH.screenRatio / 2, 0.5f, -1f, gameRH.screenRatio, 1f, "background.bmp", true, "BackGround"));
                objMainMenu.Add(new MenuObject(gameRH.screenRatio / 2, 0.5f, 1f, widthBtn, heightBtn, "startBTN.bmp", true, "StartBtn"));
                objMainMenu.Add(new MenuObject(gameRH.screenRatio / 2, 0.5f - multBtn, 1f, widthBtn, heightBtn, "helpBTN.bmp", true, "HelpBtn"));
                objMainMenu.Add(new MenuObject(gameRH.screenRatio / 2, 0.5f - multBtn * 2, 1f, widthBtn, heightBtn, "exitBTN.bmp", true, "ExitBtn"));

                loadObj = false;
            }
        }

        public override void onMouseDown(object sender, MouseButtonEventArgs e)
        {
            Point position = e.GetPosition(mainCanva);

            float X = (float)position.X / gameRH.W * gameRH.screenRatio;
            float Y = 1f - (float)position.Y / gameRH.H;

            foreach (MenuObject obj in objMainMenu)
            {
                if (obj.checkCollision(X, Y))
                {
                    objClick.Enqueue(obj.getName());
                }
            }
        }
    }
}
