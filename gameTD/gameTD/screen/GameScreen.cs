﻿using gameTD.entity;
using gameTD.entity.Enemies;
using gameTD.entity.Towers;
using gameTD.render;
using gameTD.td;
using SharpGL;
using SharpGL.SceneGraph;
using SharpGL.WPF;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;

namespace gameTD.screen
{
    class GameScreen : BaseScreen
    {
        int[,] levelMap;
        Queue<float[]> waypoints;
        float[] start;
        float[] finish;

        int money;
        double lastMove;
        double startTime;
        bool waitBuild;

        private Stopwatch stopwatch;

        //списки всех обьектов на экране
        protected List<Tile> objTiles;
        protected List<MenuObject> objMainMenu;
        protected List<Enemy> objEnemy;
        protected List<Tower> objTowers;
        protected List<Tower> objTowersMenu;


        public GameScreen(OpenGLControl mainCanva, RenderHelper gameRenderHelper, GameTD gameTD) : base(mainCanva, gameRenderHelper, gameTD)
        {
            objTiles = new List<Tile>();
            objMainMenu = new List<MenuObject>();
            waypoints = new Queue<float[]>();
            objEnemy = new List<Enemy>();
            objTowers = new List<Tower>();
            objTowersMenu = new List<Tower>();
            stopwatch = new Stopwatch();

            stopwatch.Restart();

            money = 30;
            lastMove = 0;
            startTime = stopwatch.Elapsed.TotalMilliseconds;
        }

        public override void onMouseDown(object sender, MouseButtonEventArgs e)
        {
            Point position = e.GetPosition(mainCanva);

            float X = (float)position.X / gameRH.W * gameRH.screenRatio;
            float Y = 1f - (float)position.Y / gameRH.H;

            if (waitBuild)
            {
                foreach (Tile obj in objTiles)
                {
                    if (obj.checkCollision(X, Y))
                    {
                        objTowers.Add(new ArrowTower(obj.getPosition()[0], obj.getPosition()[1], 1f, ConfigTD.PlateSize, ConfigTD.PlateSize, "tower.bmp", true, "testTower", ConfigTD.PlateSize, 0.1f, 10, 10));
                        waitBuild = false;
                        money -= 10;
                        return;
                    }
                }
            }
            if (money >= 10)
                foreach (Tower obj in objTowersMenu)
                {
                    if (obj.checkCollision(X, Y))
                    {
                        objClick.Enqueue(obj.getNameTower());
                        return;
                    }
                }
        }

        public override void OpenGLDraw(object sender, OpenGLEventArgs e)
        {
            loadObjects();
            gameProcessor();

            OpenGL gl = e.OpenGL;

            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);

            gl.MatrixMode(OpenGL.GL_MODELVIEW);
            gl.LoadIdentity();

            foreach (MenuObject obj in objMainMenu)
            {
                obj.draw(gl, gameRH);
            }
            foreach (Tile obj in objTiles)
            {
                obj.draw(gl, gameRH);
            }
            foreach (Enemy obj in objEnemy)
            {
                obj.draw(gl, gameRH);
            }

            foreach (Tower obj in objTowersMenu)
            {
                obj.draw(gl, gameRH);
            }
            foreach (Tower obj in objTowers)
            {
                obj.draw(gl, gameRH);
            }

            gl.MatrixMode(OpenGL.GL_MODELVIEW);
            gl.LoadIdentity();
            gl.Translate(gameRH.screenRatio - 0.5f, 0.95f, -6.0f);
            gl.Scale(0.05f, 0.05f, 0.05f);
            gl.DrawText3D("Times New Roman", 0.1f, 0.0f, 0.1f, "Money: " + (money).ToString());

            gl.Flush();
        }

        protected override void gameProcessor()
        {
            if (objClick.Count > 0)
            {
                string objEvent = "";
                if (objClick.TryDequeue(out objEvent))
                {
                    if (objEvent != "")
                    {
                        if (objEvent == "testTower")
                            waitBuild = true;
                    }
                }
            }
            if (stopwatch.Elapsed.TotalMilliseconds - lastMove > 10)
            {
                lastMove = stopwatch.Elapsed.TotalMilliseconds;
                foreach (Enemy obj in objEnemy)
                {
                    obj.move();
                }
            }

            foreach (Tower obj in objTowers)
            {
                obj.atack(objEnemy);
            }

            if (stopwatch.Elapsed.TotalMilliseconds - startTime > 1000)
            {
                startTime = stopwatch.Elapsed.TotalMilliseconds;
                objEnemy.Add(new Skeleton(start[0], start[1], 1f, ConfigTD.PlateSize / 2, ConfigTD.PlateSize / 2, "pacman.bmp", true, "testqwe", 10, 0.01f, 10, waypoints, finish));
            }
        }

        // загрузка уровня и расчет вейпоинтов
        private void genLevel()
        {
            levelMap = new int[,] { { 0, 0,  0, 0,  0, 0, 0,  0,  0, 0},
                                    { 0, 0,  0, 0,  0, 0, 0,  0,  0, 0},
                                    { 0, 0,  0, 0,  0, 0, 0,  0,  0, 0},
                                    { 1, 2, 10, 0, 13, 2, 2, 14,  0, 0},
                                    { 0, 0,  2, 0,  2, 0, 0, 15, 16, 0},
                                    { 0, 0,  2, 0,  2, 0, 0,  0,  2, 0},
                                    { 0, 0, 11, 2, 12, 0, 0,  0,  2, 0},
                                    { 0, 0,  0, 0,  0, 0, 0,  0,  2, 0},
                                    { 0, 0,  0, 0,  0, 0, 0,  0, 17, 3},
                                    { 0, 0,  0, 0,  0, 0, 0,  0,  0, 0}
                                    };                               //            /\
            //изьятие вейпоинтов из двумерного массива в формате координат массива ||
            Dictionary<int, int[]> waypointsDot = new Dictionary<int, int[]>();
            int maxWeyp = 0;

            for (int x = 0; x < ConfigTD.PlatesCount; x += 1)
            {
                for (int y = 0; y < ConfigTD.PlatesCount; y += 1)
                {
                    if (levelMap[y, x] >= 10)
                    {
                        waypointsDot.Add(levelMap[y, x], new int[] { x, y });
                        if (levelMap[y, x] > maxWeyp)
                            maxWeyp = levelMap[y, x];
                    }
                    if (levelMap[y, x] == 3)
                        finish = new float[] { ConfigTD.PlateSize / 2 + x * ConfigTD.PlateSize, ConfigTD.PlateSize / 2 + (ConfigTD.PlatesCount - 1 - y) * ConfigTD.PlateSize };
                    if (levelMap[y, x] == 1)
                        start = new float[] { ConfigTD.PlateSize / 2 + x * ConfigTD.PlateSize, ConfigTD.PlateSize / 2 + (ConfigTD.PlatesCount - 1 - y) * ConfigTD.PlateSize };
                }
            }
            //перевод координат вейпоинтов в OGL формат
            for (int i = 10; i <= maxWeyp; i += 1)
            {
                int x = waypointsDot[i][0];
                int y = waypointsDot[i][1];

                waypoints.Enqueue(new float[] { ConfigTD.PlateSize / 2 + x * ConfigTD.PlateSize, ConfigTD.PlateSize / 2 + (ConfigTD.PlatesCount - 1 - y) * ConfigTD.PlateSize });
            }
        }

        protected override void loadObjects()
        {
            if (loadObj)
            {
                //фон
                objMainMenu.Add(new MenuObject(gameRH.screenRatio - (gameRH.screenRatio - ConfigTD.PlateSize * ConfigTD.PlatesCount) / 2, 0.5f, -1f, (gameRH.screenRatio - ConfigTD.PlateSize * ConfigTD.PlatesCount), 1f, "background.bmp", true, "BackGround"));

                //меню пушек
                objTowersMenu.Add(new ArrowTower(gameRH.screenRatio - (gameRH.screenRatio - ConfigTD.PlateSize * ConfigTD.PlatesCount) / 2, 0.5f, 1f, ConfigTD.PlateSize, ConfigTD.PlateSize, "tower.bmp", true, "testTower", 0.2f, 0.2f, 10, 10));

                genLevel();
                //игровая область
                for (int x = 0; x < ConfigTD.PlatesCount; x += 1)
                {
                    for (int y = 0; y < ConfigTD.PlatesCount; y += 1)
                    {
                        if (levelMap[ConfigTD.PlatesCount - 1 - y, x] == 0)
                            objTiles.Add(new Tile(ConfigTD.PlateSize / 2 + x * ConfigTD.PlateSize, ConfigTD.PlateSize / 2 + y * ConfigTD.PlateSize, 1f, ConfigTD.PlateSize, ConfigTD.PlateSize, "Crate.bmp", true, 0));
                        if (levelMap[ConfigTD.PlatesCount - 1 - y, x] == 1)
                            objTiles.Add(new Tile(ConfigTD.PlateSize / 2 + x * ConfigTD.PlateSize, ConfigTD.PlateSize / 2 + y * ConfigTD.PlateSize, 1f, ConfigTD.PlateSize, ConfigTD.PlateSize, "circle.bmp", true, 1));
                        if (levelMap[ConfigTD.PlatesCount - 1 - y, x] == 2)
                            objTiles.Add(new Tile(ConfigTD.PlateSize / 2 + x * ConfigTD.PlateSize, ConfigTD.PlateSize / 2 + y * ConfigTD.PlateSize, 1f, ConfigTD.PlateSize, ConfigTD.PlateSize, "cross.bmp", true, 2));
                        if (levelMap[ConfigTD.PlatesCount - 1 - y, x] == 3)
                            objTiles.Add(new Tile(ConfigTD.PlateSize / 2 + x * ConfigTD.PlateSize, ConfigTD.PlateSize / 2 + y * ConfigTD.PlateSize, 1f, ConfigTD.PlateSize, ConfigTD.PlateSize, "circle.bmp", true, 3));
                        if (levelMap[ConfigTD.PlatesCount - 1 - y, x] >= 10)
                            objTiles.Add(new Tile(ConfigTD.PlateSize / 2 + x * ConfigTD.PlateSize, ConfigTD.PlateSize / 2 + y * ConfigTD.PlateSize, 1f, ConfigTD.PlateSize, ConfigTD.PlateSize, "cross.bmp", true, levelMap[ConfigTD.PlatesCount - 1 - y, x]));
                    }
                }

                //моб
                objEnemy.Add(new Skeleton(start[0], start[1], 1f, ConfigTD.PlateSize / 2, ConfigTD.PlateSize / 2, "pacman.bmp", true, "testqwe", 10, 0.01f, 10, waypoints, finish));
                loadObj = false;
            }
        }
    }
}
