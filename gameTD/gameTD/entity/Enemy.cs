﻿using System;
using System.Collections.Generic;

namespace gameTD.entity
{
    abstract class Enemy : Entity
    {
        protected string nameEnemy;

        protected int health;
        protected float speed;
        protected int moneyRevard;

        protected Queue<float[]> waypoints;
        protected float[] finish;

        protected float[] waypoint;


        public Enemy(float X, float Y, float Z, float Width, float Height, string texture, bool active,
            string nameEnemy, int health, float speed, int moneyRevard, Queue<float[]> waypoints, float[] finish)
            : base(X, Y, Z, Width, Height, texture, active)
        {
            this.nameEnemy = nameEnemy;
            this.health = health;
            this.speed = speed;
            this.moneyRevard = moneyRevard;
            this.waypoints = new Queue<float[]>(waypoints);
            this.finish = finish;

            nextWaypoint();
        }

        public void move()
        {
            if ((Math.Abs(waypoint[0] - this.X) < speed) & (Math.Abs(waypoint[1] - this.Y) < speed))
            {
                this.update(waypoint[0], waypoint[1]);
                nextWaypoint();
                return;
            }

            float multX, multY, mult;
            multX = Math.Abs(waypoint[0] - this.X);
            multY = Math.Abs(waypoint[1] - this.Y);

            if (multX > multY)
            {
                mult = multY / multX;
                multX = 1 - mult;
                multY = mult;
            }
            else
            {
                mult = multX / multY;
                multY = 1 - mult;
                multX = mult;
            }
            if (waypoint[0] - this.X > 0)
            {
                this.X += speed * multX;
            }
            else
            {
                this.X -= speed * multX;
            }

            if (waypoint[1] - this.Y > 0)
            {
                this.Y += speed * multY;
            }
            else
            {
                this.Y -= speed * multY;
            }

        }

        public void nextWaypoint()
        {
            if (waypoint == finish)
            {
                //минус жизни
                this.active = false;
            }
            if (waypoints.Count > 0)
                waypoint = waypoints.Dequeue();
            else
                waypoint = finish;
        }

        public void settWaypoints(Queue<float[]> waypoints)
        {
            this.waypoints = waypoints;
        }
    }
}
