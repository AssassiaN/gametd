﻿using gameTD.render;
using SharpGL;

namespace gameTD.entity
{
    class MenuObject : Entity
    {
        protected string nameObj;

        public MenuObject(float X, float Y, float Z, float Width, float Height, string texture, bool active,
            string name)
            : base(X, Y, Z, Width, Height, texture, active)
        {
            this.nameObj = name;
        }

        public string getName()
        {
            return this.nameObj;
        }
    }
}
