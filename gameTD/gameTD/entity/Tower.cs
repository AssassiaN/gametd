﻿using gameTD.td;
using System.Collections.Generic;

namespace gameTD.entity
{
    abstract class Tower : Entity
    {
        protected int damage, cost, level;
        protected float attackRange, attackCooldown;
        protected string nameTower;


        public Tower(float X, float Y, float Z, float Width, float Height, string texture, bool active, 
            string nameTower, float attackRange, float attackCooldown, int damage, int cost) 
            : base(X, Y, Z, Width, Height, texture, active)
        {
            this.damage = damage;
            this.cost = cost;
            level = 1;
            this.attackRange = attackRange;
            this.attackCooldown = attackCooldown;
            this.nameTower = nameTower;
        }

        protected int getSellCost()
        {
            return cost * ConfigTD.SellFee / 100;
        }

        protected abstract int getUpgradeCost();

        public string getNameTower()
        {
            return nameTower;
        }

        public void atack(List<Enemy> objEnemy)
        {
            foreach (Enemy tempObj in objEnemy)
            {
                float[] tempPos = tempObj.getPosition();
                if (this.checkCollision(tempPos[0], tempPos[1], attackRange))
                {
                    tempObj.setActive(false);
                }
            }
        }
    }
}
