﻿using gameTD.render;
using SharpGL;

namespace gameTD.entity
{
    abstract class Entity
    {
        //Координаты центра
        protected float X;
        protected float Y;
        protected float Z;
        protected float Width;
        protected float Height;

        protected string texture;
        protected bool active;

        public Entity(float X, float Y, float Z, float Width, float Height, string texture, bool active)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
            this.Width = Width;
            this.Height = Height;
            this.texture = texture;
            this.active = active;
        }

        public void draw(OpenGL gl, RenderHelper gameRH)
        {
            if (this.active)
            {
                gameRH.drawRectangleWH(gl, this.X, this.Y, this.Width, this.Height, this.Z, this.texture);
            }
        }

        public void update(float X, float Y)
        {
            this.X = X;
            this.Y = Y;
        }

        public void setActive(bool active)
        {
            this.active = active;
        }

        public bool checkCollision(float X, float Y)
        {
            if ((X > this.X - Width / 2) & (X < this.X + Width / 2))
            {
                if ((Y > this.Y - Height / 2) & (Y < this.Y + Height / 2))
                {
                    return true;
                }
            }
            return false;
        }

        public bool checkCollision(float X, float Y, float R)
        {
            if ((X > this.X - Width / 2 - R) & (X < this.X + Width / 2 + R))
            {
                if ((Y > this.Y - Height / 2 - R) & (Y < this.Y + Height / 2 + R))
                {
                    return true;
                }
            }
            return false;
        }


        public float[] getPosition()
        {
            return new float[] { this.X, this.Y };
        }
    }
}
