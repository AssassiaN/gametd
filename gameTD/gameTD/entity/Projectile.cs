﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameTD.entity
{
    abstract class Projectile : Entity
    {
        public Projectile(float X, float Y, float Z, float Width, float Height, string texture, bool active) : base(X, Y, Z, Width, Height, texture, active)
        {
        }
    }
}
