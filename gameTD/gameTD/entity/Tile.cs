﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gameTD.render;
using SharpGL;

namespace gameTD.entity
{
    class Tile : Entity
    {
        //0 - можно строить
        //1 - старт, нельзя строить
        //2 - дорога, нельзя строить
        //3 - финиш, нельзя строить
        //10..999 - вейпоинты то же что и дорога но указывает вектор движения моба
        protected int type;
        public Tile(float X, float Y, float Z, float Width, float Height, string texture, bool active,int type) : base(X, Y, Z, Width, Height, texture, active)
        {
            this.type = type;
        }
    }
}
