﻿using System.Collections.Generic;

namespace gameTD.entity.Enemies
{
    class Skeleton : Enemy
    {
        public Skeleton(float X, float Y, float Z, float Width, float Height, string texture, bool active, string nameEnemy, int health, float speed, int moneyRevard, Queue<float[]> waypoints, float[] finish) : base(X, Y, Z, Width, Height, texture, active, nameEnemy, health, speed, moneyRevard, waypoints, finish)
        {
        }
    }
}
