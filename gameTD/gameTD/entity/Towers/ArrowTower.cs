﻿using System;
using gameTD.render;
using SharpGL;
using gameTD.td;

namespace gameTD.entity.Towers
{
    class ArrowTower : Tower
    {
        public ArrowTower(float X, float Y, float Z, float Width, float Height, string texture, bool active, string nameTower, float attackRange, float attackCooldown, int damage, int cost) : base(X, Y, Z, Width, Height, texture, active, nameTower, attackRange, attackCooldown, damage, cost)
        {
        }

        protected override int getUpgradeCost()
        {
            return (int)(cost * ConfigTD.ArrowTowerCostMult / 100);
        }
    }
}
