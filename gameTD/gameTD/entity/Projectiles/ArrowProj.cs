﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gameTD.render;
using SharpGL;

namespace gameTD.entity.Projectiles
{
    class ArrowProj : Projectile
    {
        public ArrowProj(float X, float Y, float Z, float Width, float Height, string texture, bool active) : base(X, Y, Z, Width, Height, texture, active)
        {
        }
    }
}
