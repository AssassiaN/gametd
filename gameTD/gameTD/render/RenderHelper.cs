﻿using gameTD.td;
using SharpGL;
using SharpGL.SceneGraph;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace gameTD.render
{
    class RenderHelper
    {
        private Drawer drawer;
        //Таймер для подсчета фпс
        private Stopwatch stopwatch;
        //время последнего кадра
        private double frameTime;
        //ширина окна
        public float W;
        //высота окна
        public float H;
        //соотношение сторн, крайняя точка по Х
        public float screenRatio;


        public RenderHelper()
        {
            stopwatch = new Stopwatch();
        }

        public void setDrawer(Drawer drawer)
        {
            this.drawer = drawer;
        }

        public void openGLDraw(object sender, OpenGLEventArgs e)
        {
            OpenGL gl = e.OpenGL;

            stopwatch.Restart();

            drawer.OpenGLDraw(sender, e);

            //отрисовка фпс
            if (ConfigTD.DrawFPS)
            {
                gl.MatrixMode(OpenGL.GL_MODELVIEW);
                gl.LoadIdentity();
                gl.Translate(0f, 0.95f, -6.0f);
                gl.Scale(0.05f, 0.05f, 0.05f);
                gl.DrawText3D("Times New Roman", 0.1f, 0.0f, 0.1f, (1000.0 / frameTime).ToString("0.0") + " FPS");
            }

            stopwatch.Stop();

            frameTime = stopwatch.Elapsed.TotalMilliseconds;
        }

        //конфигурация OGL и загрузка текстур
        public void openGLInitialized(object sender, OpenGLEventArgs e)
        {
            OpenGL gl = e.OpenGL;

            W = (float)gl.RenderContextProvider.Width;
            H = (float)gl.RenderContextProvider.Height;
            screenRatio = W / H;

            gl.Enable(OpenGL.GL_TEXTURE_2D);

            loadTextures(gl);
        }

        //имя текстуры -> id текстуры в OGL.
        Dictionary<string, uint> texturesDict = new Dictionary<string, uint>();

        //масштабирование и приведение к стандартным декардовым координатам (0:0 слева внизу)
        public void openGLResized(object sender, OpenGLEventArgs e)
        {
            OpenGL gl = e.OpenGL;

            W = (float)gl.RenderContextProvider.Width;
            H = (float)gl.RenderContextProvider.Height;
            screenRatio = W / H;

            gl.Disable(OpenGL.GL_DEPTH_TEST);

            gl.MatrixMode(OpenGL.GL_PROJECTION);
            gl.LoadIdentity();

            gl.Viewport(0, 0, (int)W, (int)H);
            gl.Ortho(0f, screenRatio, 0f, 1f, -1f, 10f);
        }

        //Отрисовка прямоугольника, на вход левый нижний и правый верхний угол
        public void drawRectangle(OpenGL gl, float xLeft, float yLeft, float xRight, float yRight, float z, string textureName)
        {
            //Выбор текстуры.
            gl.BindTexture(OpenGL.GL_TEXTURE_2D, texturesDict[textureName]);

            gl.Begin(OpenGL.GL_TRIANGLES);
            gl.TexCoord(0.0f, 1.0f); gl.Vertex(xLeft, yLeft, z);             // левый нижний
            gl.TexCoord(1.0f, 1.0f); gl.Vertex(xRight, yLeft, z);            // правый нижний
            gl.TexCoord(1.0f, 0.0f); gl.Vertex(xRight, yRight, z);           // правый верхний

            gl.TexCoord(1.0f, 0.0f); gl.Vertex(xRight, yRight, z);           // правый верхний
            gl.TexCoord(0.0f, 0.0f); gl.Vertex(xLeft, yRight, z);            // левый верхний
            gl.TexCoord(0.0f, 1.0f); gl.Vertex(xLeft, yLeft, z);             // левый нижний
            gl.End();

            gl.BindTexture(OpenGL.GL_TEXTURE_2D, 0);
        }

        //Отрисовка прямоугольника, на вход центр и ширина/высота
        public void drawRectangleWH(OpenGL gl, float X, float Y, float Width, float Height, float z, string textureName)
        {
            //Выбор текстуры.
            gl.BindTexture(OpenGL.GL_TEXTURE_2D, texturesDict[textureName]);

            gl.Begin(OpenGL.GL_TRIANGLES);
            gl.TexCoord(0.0f, 1.0f); gl.Vertex(X - Width / 2, Y - Height / 2, z);             // левый нижний
            gl.TexCoord(1.0f, 1.0f); gl.Vertex(X + Width / 2, Y - Height / 2, z);             // правый нижний
            gl.TexCoord(1.0f, 0.0f); gl.Vertex(X + Width / 2, Y + Height / 2, z);             // правый верхний

            gl.TexCoord(1.0f, 0.0f); gl.Vertex(X + Width / 2, Y + Height / 2, z);             // правый верхний
            gl.TexCoord(0.0f, 0.0f); gl.Vertex(X - Width / 2, Y + Height / 2, z);             // левый верхний
            gl.TexCoord(0.0f, 1.0f); gl.Vertex(X - Width / 2, Y - Height / 2, z);             // левый нижний
            gl.End();

            gl.BindTexture(OpenGL.GL_TEXTURE_2D, 0);
        }

        void loadTextures(OpenGL gl)
        {
            //Получение списка файлов текстур для зугрузки
            string pathRes = Directory.GetCurrentDirectory() + System.IO.Path.DirectorySeparatorChar + "res";
            string[] fileEntries = Directory.GetFiles(pathRes);
            int texturesCount = fileEntries.Length;

            // Получение id текстур.
            uint[] texturesID = new uint[texturesCount];
            gl.GenTextures(texturesCount, texturesID);

            //загрузка текстур
            for (int i = 0; i < texturesCount; i++)
            {
                Bitmap textureImage = new Bitmap(fileEntries[i]);

                gl.BindTexture(OpenGL.GL_TEXTURE_2D, texturesID[i]);
                texturesDict.Add(System.IO.Path.GetFileName(fileEntries[i]), texturesID[i]);

                Rectangle rect = new Rectangle(0, 0, textureImage.Width, textureImage.Height);
                BitmapData bmpData = textureImage.LockBits(rect, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                gl.TexImage2D(OpenGL.GL_TEXTURE_2D, 0, OpenGL.GL_RGB, textureImage.Width, textureImage.Height, 0, OpenGL.GL_BGR, OpenGL.GL_UNSIGNED_BYTE, bmpData.Scan0);

                textureImage.UnlockBits(bmpData);

                gl.TexParameter(OpenGL.GL_TEXTURE_2D, OpenGL.GL_TEXTURE_MIN_FILTER, OpenGL.GL_LINEAR);
                gl.TexParameter(OpenGL.GL_TEXTURE_2D, OpenGL.GL_TEXTURE_MAG_FILTER, OpenGL.GL_LINEAR);
            }

            gl.BindTexture(OpenGL.GL_TEXTURE_2D, 0);
        }
    }
}
