﻿using SharpGL.SceneGraph;

namespace gameTD.render
{
    public interface Drawer
    {
        void OpenGLDraw(object sender, OpenGLEventArgs e);
    }
}
