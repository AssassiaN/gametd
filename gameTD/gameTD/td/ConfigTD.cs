﻿namespace gameTD.td
{
    class ConfigTD
    {
        //глобальные константы

        public static int PlatesCount = 10;

        public static float PlateSize = 1f / PlatesCount;

        public static bool DrawFPS = true;

        public static int SellFee = 50;

        public static float ArrowTowerCostMult = 30;
        public static float ArrowTowerDMGMult = 10;
        public static float ArrowTowerSpeedMult = 10;
    }
}
