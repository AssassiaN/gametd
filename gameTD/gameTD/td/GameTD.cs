﻿using gameTD.render;
using gameTD.screen;
using SharpGL.WPF;
using System;

namespace gameTD.td
{
    class GameTD
    {
        //opengl полотно
        OpenGLControl mainCanva;
        // "библиотека" для отрисовки
        RenderHelper gameRenderHelper;

        //экраны
        private MainMenuScreen gameMainMenuScreen;
        private GameScreen gameGameScreen;

        //Загрузка и подключение экранов
        public GameTD(OpenGLControl mainCanva)
        {
            this.mainCanva = mainCanva;

            gameRenderHelper = new RenderHelper();

            gameMainMenuScreen = new MainMenuScreen(this.mainCanva, gameRenderHelper, this);
            gameGameScreen = new GameScreen(this.mainCanva, gameRenderHelper, this);

            this.mainCanva.OpenGLDraw += gameRenderHelper.openGLDraw;
            this.mainCanva.OpenGLInitialized += gameRenderHelper.openGLInitialized;
            this.mainCanva.Resized += gameRenderHelper.openGLResized;

            changeScreen(gameMainMenuScreen);
        }

        //смена экранов
        public void changeScreen(BaseScreen gameScreen)
        {
            gameRenderHelper.setDrawer(gameScreen);
            mainCanva.MouseDown += gameScreen.onMouseDown;
        }

        public BaseScreen getMainMenuScreen()
        {
            return gameMainMenuScreen;
        }

        public BaseScreen getGameScreen()
        {
            return gameGameScreen;
        }

        public void exitTD()
        {
            Environment.Exit(0);
        }
    }
}
