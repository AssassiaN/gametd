﻿using System.Windows;

using gameTD.td;

namespace gameTD
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        GameTD gameTD;
        public MainWindow()
        {
            InitializeComponent();
            gameTD = new GameTD(MainOGLControl);
        }
    }
}